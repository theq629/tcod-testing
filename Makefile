CHECKOUT := $(PWD)/libtcod
BUILD = $(PWD)/build

all: build build-samples build-events-tracer

prepare: .FORCE
	cd $(CHECKOUT)/buildsys/autotools/; \
		autoreconf -i; \
		./configure --prefix="$(BUILD)"

build: .FORCE
	cd $(CHECKOUT)/buildsys/autotools/; \
		make; \
		make install

build-samples-c: build
	cd $(CHECKOUT)/samples/; \
		gcc -lm -I/usr/include/SDL2 -lSDL2 samples_c.c -I$(BUILD)/include -L$(BUILD)/lib -ltcod -o samples_c

build-samples-cpp: build
	cd $(CHECKOUT)/samples/; \
		g++ -I/usr/include/SDL2 -lSDL2 samples_cpp.cpp -I$(BUILD)/include -L$(BUILD)/lib -ltcod -o samples_cpp

build-samples-rad: build
	cd $(CHECKOUT)/samples/rad; \
		g++ -I/usr/include/SDL2 -lSDL2 *.cpp -I$(BUILD)/include -L$(BUILD)/lib -ltcod -o rad

build-samples: build-samples-c build-samples-cpp build-samples-rad

build-events-tracer: build
	cd eventstracer/; \
		gcc -lm -I/usr/include/SDL2 -lSDL2 *.c -I$(BUILD)/include -L$(BUILD)/lib -ltcod -o eventstracer

run: build-samples
	cd $(CHECKOUT); \
		./samples/a.out

clean:
	rm -rf build
	cd $(CHECKOUT) && git clean -fd

.FORCE:
