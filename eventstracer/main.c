#include <SDL.h>
#include <libtcod.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void fatal(const char* format, ...) {
  va_list args;
  va_start(args, format);
  vfprintf(stderr, format, args);
  va_end(args);
  exit(EXIT_FAILURE);
}

void show_size(TCOD_Context *context, FILE *log_file) {
  int cols, rows;
  TCOD_context_recommended_console_size(context, 1.0, &cols, &rows);
	fprintf(log_file, "recommended console size %i, %i\n", cols, rows);
	fflush(log_file);
}

int watch(TCOD_Context *context, TCOD_Console *console, FILE *log_file) {
  while (true) {
    TCOD_console_set_default_background(console, TCOD_black);
    TCOD_console_clear(console);
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
        case SDL_KEYDOWN:
          fprintf(log_file,
              "SDL_KEYDOWN ts %i state %i repeat %i sym %i name %s scancode %i mod %i\n",
              event.key.timestamp,
              event.key.state,
              event.key.repeat, 
              event.key.keysym.sym,
              SDL_GetKeyName(event.key.keysym.sym),
              event.key.keysym.scancode,
              event.key.keysym.mod
              );
          fflush(log_file);
          switch (event.key.keysym.scancode) {
            case SDL_SCANCODE_S:
							show_size(context, log_file);
              break;
            case SDL_SCANCODE_Q:
              return 0;
          }
          break;
        case SDL_KEYUP:
          fprintf(log_file,
              "SDL_KEYUP ts %i state %i repeat %i sym %i name %s scancode %i mod %i\n",
              event.key.timestamp,
              event.key.state,
              event.key.repeat, 
              event.key.keysym.sym,
              SDL_GetKeyName(event.key.keysym.sym),
              event.key.keysym.scancode,
              event.key.keysym.mod
              );
          fflush(log_file);
        case SDL_TEXTINPUT:
          fprintf(log_file,
              "SDL_TEXTINPUT ts %i text '%s'\n",
              event.text.timestamp,
              event.text.text
              );
          fflush(log_file);
          break;
        case SDL_WINDOWEVENT:
          switch (event.window.event) {
            case SDL_WINDOWEVENT_RESIZED:
              fprintf(log_file,
                  "SDL_WINDOWEVENT ts %i event SDL_WINDOWEVENT_RESIZED data1 %i data2 %i\n",
                  event.window.timestamp,
                  event.window.data1,
                  event.window.data2
                  );
              fflush(log_file);
              break;
            case SDL_WINDOWEVENT_FOCUS_GAINED:
              fprintf(log_file,
                  "SDL_WINDOWEVENT ts %i event SDL_WINDOWEVENT_FOCUS_GAINED\n",
                  event.window.timestamp
                  );
              fflush(log_file);
              break;
            case SDL_WINDOWEVENT_FOCUS_LOST:
              fprintf(log_file,
                  "SDL_WINDOWEVENT ts %i event SDL_WINDOWEVENT_FOCUS_LOST\n",
                  event.window.timestamp
                  );
              fflush(log_file);
              break;
            default:
              fprintf(log_file,
                  "SDL_WINDOWEVENT ts %i event %i data1 %i data2 %i\n",
                  event.window.timestamp,
                  event.window.event,
                  event.window.data1,
                  event.window.data2
                  );
              fflush(log_file);
          }
          break;
        case SDL_MOUSEMOTION:
          fprintf(log_file,
              "SDL_MOUSEMOTION ts %i which %i state %i x %i y %i xrel %i yrel %i\n",
              event.motion.timestamp,
              event.motion.which,
              event.motion.state,
              event.motion.x,
              event.motion.y,
              event.motion.xrel,
              event.motion.yrel
              );
          fflush(log_file);
          break;
        case SDL_MOUSEBUTTONDOWN:
          fprintf(log_file,
              "SDL_MOUSEBUTTONDOWN ts %i which %i but %i state %i clicks %i x %i y %i\n",
              event.button.timestamp,
              event.button.which,
              event.button.button,
              event.button.state,
              event.button.clicks,
              event.button.x,
              event.button.y
              );
          fflush(log_file);
          break;
        case SDL_MOUSEBUTTONUP:
          fprintf(log_file,
              "SDL_MOUSEBUTTONUP ts %i which %i but %i state %i clicks %i x %i y %i\n",
              event.button.timestamp,
              event.button.which,
              event.button.button,
              event.button.state,
              event.button.clicks,
              event.button.x,
              event.button.y
              );
          fflush(log_file);
          break;
        case SDL_MOUSEWHEEL:
          fprintf(log_file,
              "SDL_MOUSEWHEEL ts %i which %i i x %i y %i dir %i\n",
              event.wheel.timestamp,
              event.wheel.which,
              event.wheel.x,
              event.wheel.y,
              event.wheel.direction
              );
          fflush(log_file);
          break;
        case SDL_QUIT:
          fprintf(log_file, "SDL_QUIT\n");
          fflush(log_file);
          return EXIT_SUCCESS;  // Exit program by returning from main.
        default:
          fprintf(log_file, "other event %i\n", event.type);
      }
    }
  }
}

int main(int argc, char* argv[]) {
  static const char* FONT = "data/fonts/dejavu10x10_gs_tc.png";
  TCOD_Tileset* tileset = TCOD_tileset_load(FONT, 32, 8, 256, TCOD_CHARMAP_TCOD);
  if (!tileset) fatal("Could not load font %s: %s", FONT, TCOD_get_error());
  TCOD_Console* main_console = TCOD_console_new(80, 50);  // The main console to be presented.
  if (!main_console) fatal("Could not allocate console: %s", TCOD_get_error());
  TCOD_ContextParams params = {
      .tcod_version = TCOD_COMPILEDVERSION,
      .console = main_console,
      .window_title = "libtcod C sample",
      .sdl_window_flags = SDL_WINDOW_RESIZABLE,
      .renderer_type = TCOD_RENDERER_SDL2,
      .tileset = tileset,
      .vsync = false,
      .argc = argc,
      .argv = argv,
  };

  TCOD_Context* context;
  if (TCOD_context_new(&params, &context) < 0) fatal("Could not open context: %s", TCOD_get_error());
  atexit(TCOD_quit);

  FILE *log_file = fopen("events.log", "w");
	show_size(context, log_file);
  int ret = watch(context, main_console, log_file);
  fclose(log_file);

  TCOD_context_delete(context);
  return ret;
}
